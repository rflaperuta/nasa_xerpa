import pytest
import mars_grid


class TestGrid(object):
    def test_simple(self):
        mars_grid.Grid("Test Grid", 10, 10)

    def test_invalid_grid_x(self):
        with pytest.raises(mars_grid.GridBoundaryError):
            mars_grid.Grid("Test Invalid Grid X", 0, 10)

    def test_invalid_grid_y(self):
        with pytest.raises(mars_grid.GridBoundaryError):
            mars_grid.Grid("Test Invalid Grid Y", 40, 0)

    def test_negative_grid_x(self):
        with pytest.raises(mars_grid.GridBoundaryError):
            mars_grid.Grid("Test Negative Grid X", -1, 10)

    def test_negative_grid_y(self):
        with pytest.raises(mars_grid.GridBoundaryError):
            mars_grid.Grid("Test Negative Grid Y", 10, -1)
