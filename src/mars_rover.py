import random

_VALID_ORIENTATIONS = frozenset({"N", "E", "S", "W"})
_VALID_INSTRUCTIONS = frozenset({"L", "R", "M", "P"})
_MARS_ARCHEOLOGY = [
    "a Monolith",
    "an Abandoned Alien Site",
    "some Alien Fossils",
    "Live Aliens!",
]


class RoverStartPositionError(ValueError):
    pass


class RoverOrientationError(ValueError):
    pass


class RoverInstructionsError(ValueError):
    pass


class Rover(object):
    def __init__(self, pos_x, pos_y, orientation, instructions):
        if pos_x < 0:
            raise RoverStartPositionError(
                "Invalid Rover X Start Position {}".format(pos_x)
            )
        if pos_y < 0:
            raise RoverStartPositionError(
                "Invalid Rover Y Start Position {}".format(pos_y)
            )
        if orientation not in _VALID_ORIENTATIONS:
            raise RoverOrientationError(
                "Invalid Rover Start Orientation {}".format(orientation)
            )
        if not set(instructions).issubset(_VALID_INSTRUCTIONS):
            raise RoverInstructionsError(
                "Invalid Rover Instructions {}".format(instructions)
            )

        self.start_x = pos_x
        self.start_y = pos_y
        self.start_orientation = orientation
        self.instructions = instructions
        self.current_x = self.start_x
        self.current_y = self.start_y
        self.current_orientation = self.start_orientation
        self._instruction_count = len(self.instructions)
        self._starting_chance = random.randint(1, self._instruction_count + 1)
        self._movement_chance = 0

    def __str__(self):
        return f"{self.current_x} {self.current_y} {self.current_orientation}"

    def __repr__(self):
        return (
            f"{self.__class__.__name__}("
            f"{self.start_x!r}, {self.start_y!r}, {self.start_orientation!r}, {self.instructions!r})"
        )

    def get_rover_data(self):
        return (
            f"Rover#: Initial POS ({self.start_x}, {self.start_y}, '{self.start_orientation}')"
            f" - Current POS ({self.current_x}, {self.current_y}, '{self.current_orientation}')"
            f" - Instructions [{self.instructions}]"
        )

    def _turn_right(self):
        if self.current_orientation == "N":
            self.current_orientation = "E"
        elif self.current_orientation == "E":
            self.current_orientation = "S"
        elif self.current_orientation == "S":
            self.current_orientation = "W"
        elif self.current_orientation == "W":
            self.current_orientation = "N"

    def _turn_left(self):
        if self.current_orientation == "N":
            self.current_orientation = "W"
        elif self.current_orientation == "W":
            self.current_orientation = "S"
        elif self.current_orientation == "S":
            self.current_orientation = "E"
        elif self.current_orientation == "E":
            self.current_orientation = "N"

    def change_orientation(self, instruction):
        if instruction == "L":
            self._turn_left()
        else:
            self._turn_right()

    def _walk(self):
        if self.current_orientation == "N":
            self.current_y = self.current_y + 1
        elif self.current_orientation == "E":
            self.current_x = self.current_x + 1
        elif self.current_orientation == "S":
            self.current_y = self.current_y - 1
        elif self.current_orientation == "W":
            self.current_x = self.current_x - 1

    def _probe(self):
        if self._movement_chance == self._starting_chance:
            print(
                "Found {}!".format(
                    _MARS_ARCHEOLOGY[random.randint(0, len(_MARS_ARCHEOLOGY) - 1)]
                )
            )

    def move(self):
        for instruction in self.instructions:
            self._movement_chance = random.randint(1, self._instruction_count + 1)
            print(self.get_rover_data())
            if instruction == "L" or instruction == "R":
                self.change_orientation(instruction)
            elif instruction == "M":
                self._walk()
            elif instruction == "P":
                self._probe()
