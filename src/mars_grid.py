import mars_rover


class GridBoundaryError(ValueError):
    pass


class GridInvalidRoverError(ValueError):
    pass


class Grid(object):
    """Mars Plateu Exploration Grid Representation"""

    def __init__(self, name, size_x, size_y):
        self.name = name
        if size_x <= 0:
            raise GridBoundaryError(size_x)
        if size_y <= 0:
            raise GridBoundaryError(size_y)

        self.__size_x = size_x
        self.__size_y = size_y
        self.__rover_list = []

    def __str__(self):
        return (
            f"Grid Name: {self.name} - Upper Right: ({self.__size_x}, {self.__size_y}) - "
            f"Rovers on Grid: {self.__rover_list}"
        )

    def __repr__(self):
        return (
            f"{self.__class__.__name__}("
            f"'{self.name!r}', {self.__size_x!r}, {self.__size_y!r})"
        )

    def add_rover(self, rover):
        if rover is None:
            raise GridInvalidRoverError("Cannot Add Invalid Rover")
        self.__rover_list.append(rover)

    def move_rovers(self):
        if len(self.__rover_list) > 0:
            print("Moving Rover(s) on {}".format(self.name))
        for rover in self.__rover_list:
            rover.move()
            print(rover)

    def show_rovers_data(self):
        for rover in self.__rover_list:
            print(rover.get_rover_data())
