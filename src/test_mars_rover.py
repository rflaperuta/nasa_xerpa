import pytest
import mars_rover


class TestRover(object):
    def test_simple(self):
        mars_rover.Rover(0, 0, "N", "MLRMLR")

    def test_invalid_pos_x(self):
        with pytest.raises(mars_rover.RoverStartPositionError):
            mars_rover.Rover(-1, 0, "N", "MLRMLR")

    def test_invalid_pos_y(self):
        with pytest.raises(mars_rover.RoverStartPositionError):
            mars_rover.Rover(0, -1, "N", "MLRMLR")

    def test_invalid_facing_pos(self):
        with pytest.raises(mars_rover.RoverOrientationError):
            mars_rover.Rover(0, 0, "Z", "MLRMLR")

    def test_invalid_commands(self):
        with pytest.raises(mars_rover.RoverInstructionsError):
            mars_rover.Rover(0, 0, "N", "ASD")
