#
# NASA Mars Exploration Probe Test Bed
# Rodrigo Falcetta Laperuta <rflaperuta@gmail.com> 2018
# Part of Nasa and Xerpa colaboration agreement
#

import sys
import mars_grid
import mars_rover


def main_sequence(testfile):
    """Poor man's parser"""
    test_sequences = []
    with open(testfile, "r") as infile:
        line = infile.readline().strip()
        while line:
            if line.startswith("####"):
                grid_name = line.strip("#")
                start_x, start_y = [
                    int(pos) for pos in infile.readline().strip().split()
                ]
                current_grid = mars_grid.Grid(grid_name, start_x, start_y)
                test_sequences.append(current_grid)
                line = infile.readline().strip()
            else:
                x, y, facing = line.strip().split()
                rover_x, rover_y, rover_facing = (int(x), int(y), facing)
                rover_instructions = infile.readline().strip()
                test_sequences[-1].add_rover(
                    mars_rover.Rover(rover_x, rover_y, rover_facing, rover_instructions)
                )
                line = infile.readline().strip()

    for test in test_sequences:
        test.move_rovers()


if __name__ == "__main__":
    sys.stdout.write(
        "=== NASA Mars Exploration Probe Test Bed - Rodrigo Falcetta Laperuta - rflaperuta_@_gmail.com ===\n\n"
    )
    if len(sys.argv) < 2:
        sys.stderr.write("Usage: {} test_case_file.dat\n".format(sys.argv[0]))
        sys.exit(1)

    main_sequence(sys.argv[1])
